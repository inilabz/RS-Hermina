-- Adminer 4.5.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `tbl_absen`;
CREATE TABLE `tbl_absen` (
  `id_absen` int(11) NOT NULL AUTO_INCREMENT,
  `id_kelas` int(11) NOT NULL,
  `tanggal_absen` datetime NOT NULL,
  `status_absen` int(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_at` datetime NOT NULL,
  `delete_by` int(11) NOT NULL,
  `delete_at` datetime NOT NULL,
  PRIMARY KEY (`id_absen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_absen` (`id_absen`, `id_kelas`, `tanggal_absen`, `status_absen`, `created_by`, `created_at`, `update_by`, `update_at`, `delete_by`, `delete_at`) VALUES
(11,	1,	'2019-08-24 01:44:10',	1,	1,	'2019-08-24 01:44:10',	0,	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00'),
(12,	1,	'2019-08-24 01:45:45',	1,	1,	'2019-08-24 01:45:45',	0,	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00'),
(13,	1,	'2019-08-24 01:46:11',	1,	1,	'2019-08-24 01:46:11',	0,	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00'),
(14,	2,	'2019-08-24 01:47:21',	1,	1,	'2019-08-24 01:47:21',	0,	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `tbl_instansi`;
CREATE TABLE `tbl_instansi` (
  `id_instansi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_instansi` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0=delete,1=active',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_at` datetime NOT NULL,
  `delete_by` int(11) NOT NULL,
  `delete_at` datetime NOT NULL,
  PRIMARY KEY (`id_instansi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_instansi` (`id_instansi`, `nama_instansi`, `status`, `created_by`, `created_at`, `update_by`, `update_at`, `delete_by`, `delete_at`) VALUES
(1,	'Nama 1',	1,	1,	'2019-08-19 06:12:57',	1,	'2019-08-19 06:54:43',	1,	'2019-08-19 09:15:58'),
(2,	'Nama 2',	1,	1,	'2019-08-19 06:13:56',	1,	'2019-08-19 08:53:20',	1,	'2019-08-19 09:16:04'),
(3,	'Nama 3',	1,	1,	'2019-08-19 06:19:45',	1,	'2019-08-19 08:53:28',	1,	'2019-08-19 09:16:06'),
(4,	'Nama 4',	1,	1,	'2019-08-19 06:19:51',	1,	'2019-08-19 08:53:37',	1,	'2019-08-19 09:16:09');

DROP TABLE IF EXISTS `tbl_kelas`;
CREATE TABLE `tbl_kelas` (
  `id_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kelas` varchar(255) NOT NULL,
  `durasi_kelas` int(5) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `update_by` int(5) NOT NULL,
  `update_at` datetime NOT NULL,
  `delete_by` int(5) NOT NULL,
  `delete_at` datetime NOT NULL,
  PRIMARY KEY (`id_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_kelas` (`id_kelas`, `nama_kelas`, `durasi_kelas`, `status`, `created_by`, `created_at`, `update_by`, `update_at`, `delete_by`, `delete_at`) VALUES
(1,	'kelas 1 A',	20,	1,	1,	'2019-08-19 09:09:05',	1,	'2019-08-19 09:13:53',	0,	'0000-00-00 00:00:00'),
(2,	'kelas 1 B',	10,	1,	1,	'2019-08-19 09:12:06',	1,	'2019-08-19 09:14:32',	0,	'0000-00-00 00:00:00'),
(3,	'kelas 2',	20,	1,	1,	'2019-08-19 11:45:00',	0,	'0000-00-00 00:00:00',	1,	'2019-08-20 06:25:53');

DROP TABLE IF EXISTS `tbl_related`;
CREATE TABLE `tbl_related` (
  `user_id` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `id_kelas` (`id_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_related` (`user_id`, `id_kelas`) VALUES
(8,	1),
(8,	2),
(9,	1),
(9,	2),
(9,	3),
(6,	1),
(7,	1),
(7,	2);

DROP TABLE IF EXISTS `tbl_related_absen`;
CREATE TABLE `tbl_related_absen` (
  `id_absen` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `id_pengajar` int(11) NOT NULL,
  `keterangan` varchar(5) NOT NULL COMMENT '1=Alpha,2=Ijin,3=Sakit,4=hadir,5=belum ada keterangan',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_related_absen` (`id_absen`, `id_karyawan`, `id_pengajar`, `keterangan`, `date_add`) VALUES
(12,	6,	1,	'M',	'2019-08-24 01:45:45'),
(12,	7,	1,	'M',	'2019-08-24 01:45:45'),
(12,	8,	1,	'M',	'2019-08-24 01:45:45'),
(13,	6,	1,	'M',	'2019-08-24 01:46:11'),
(13,	7,	1,	'M',	'2019-08-24 01:46:11'),
(13,	8,	1,	'N',	'2019-08-24 01:46:11'),
(14,	7,	1,	'M',	'2019-08-24 01:47:21'),
(14,	8,	1,	'M',	'2019-08-24 01:47:21');

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_nik` varchar(50) NOT NULL,
  `user_nama` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_show_pass` varchar(255) NOT NULL,
  `user_phone` varchar(30) NOT NULL,
  `user_phone_rumah` varchar(30) NOT NULL,
  `user_gender` tinyint(1) NOT NULL COMMENT '0=wanita,1=pria',
  `user_alamat` text NOT NULL,
  `user_tempat_lahir` varchar(255) NOT NULL,
  `user_tanggal_lahir` date NOT NULL,
  `user_status` tinyint(1) NOT NULL COMMENT '0=block,1=blm_akif,2=aktif',
  `user_status_login` tinyint(1) NOT NULL COMMENT '1=member,2=admin,3=pengajar',
  `user_photo` varchar(255) NOT NULL,
  `user_unik_id` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_at` datetime NOT NULL,
  `delete_by` int(11) NOT NULL,
  `delete_at` datetime NOT NULL,
  `id_instansi` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `id_instansi` (`id_instansi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_user` (`user_id`, `user_nik`, `user_nama`, `user_email`, `user_password`, `user_show_pass`, `user_phone`, `user_phone_rumah`, `user_gender`, `user_alamat`, `user_tempat_lahir`, `user_tanggal_lahir`, `user_status`, `user_status_login`, `user_photo`, `user_unik_id`, `created_by`, `created_at`, `update_by`, `update_at`, `delete_by`, `delete_at`, `id_instansi`) VALUES
(1,	'123456',	'bernardinus kunto wijoyo',	'inilabz@gmail.com',	'21232f297a57a5a743894a0e4a801fc3',	'',	'085777099961',	'',	1,	'',	'',	'2000-02-08',	2,	2,	'',	'',	0,	'0000-00-00 00:00:00',	1,	'2019-08-17 03:48:27',	1,	'2019-08-19 06:33:49',	NULL),
(6,	'12345',	'kunto 1',	'k@gmail.com',	'e10adc3949ba59abbe56e057f20f883e',	'',	'085777',	'',	0,	'',	'',	'1910-06-16',	2,	1,	'',	'',	1,	'2019-08-19 10:38:10',	1,	'2019-08-24 02:00:27',	0,	'0000-00-00 00:00:00',	3),
(7,	'12345',	'kunto 2',	'k1@gmail.com',	'e10adc3949ba59abbe56e057f20f883e',	'',	'085777',	'',	0,	'',	'',	'1970-03-06',	2,	1,	'',	'',	1,	'2019-08-19 10:39:16',	1,	'2019-08-24 02:00:35',	0,	'0000-00-00 00:00:00',	1),
(8,	'12345',	'bernardinus kunto wijoyo',	'bernardinsuswijaya@gmail.com',	'e10adc3949ba59abbe56e057f20f883e',	'',	'1231423',	'',	0,	'',	'',	'0000-00-00',	2,	1,	'',	'',	1,	'2019-08-19 10:40:00',	1,	'2019-08-20 12:25:31',	0,	'0000-00-00 00:00:00',	3),
(9,	'1231231312',	'Dosen 1',	'dosen@gmail.com',	'e10adc3949ba59abbe56e057f20f883e',	'',	'085777099961',	'',	0,	'asdasd',	'',	'2019-08-27',	2,	3,	'1c99ede688f0803e19c0c1786f5ff04d.png',	'',	1,	'2019-08-20 06:35:03',	1,	'2019-08-20 11:25:29',	1,	'2019-08-20 06:39:18',	2);

DROP TABLE IF EXISTS `tbl_user_access`;
CREATE TABLE `tbl_user_access` (
  `user_access_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_access_name` varchar(255) NOT NULL,
  `user_access` text NOT NULL,
  `user_access_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=hapus,1=show',
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_at` datetime NOT NULL,
  `deleted_by` int(11) NOT NULL,
  PRIMARY KEY (`user_access_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_user_access` (`user_access_id`, `user_access_name`, `user_access`, `user_access_status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
(1,	'superuser',	'',	1,	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00',	0,	'0000-00-00 00:00:00',	0);

DROP TABLE IF EXISTS `tbl_user_group`;
CREATE TABLE `tbl_user_group` (
  `user_id` int(11) NOT NULL,
  `user_access_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`user_access_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_user_group` (`user_id`, `user_access_id`) VALUES
(1,	1),
(6,	1);

-- 2019-08-23 19:15:58
