<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'welcome';


//backend
$route['backend/security/login'] = "backend/login/form_login";
$route['backend/pro/login'] = "backend/login/proses_login";
$route['backend/dashboard'] = "backend/home";
$route['backend/logout'] = "backend/login/logout";


//section
$route['backend/section'] = 'backend/section';
$route['backend/section/sub-menu/(:num)'] = 'backend/section';
$route['backend/section/add/(:num)'] = 'backend/section/view_form';
$route['backend/section/update/(:num)'] = 'backend/section/view_form';
$route['backend/section/proses'] = 'backend/section/proses';
$route['backend/section/proses_hapus_data'] = 'backend/section/proses_hapus_data';

//master-lingkungan
$route['backend/master-lingkungan'] = 'backend/lingkungan';
$route['backend/ketua-lingkungan'] = 'backend/lingkungan';
$route['backend/master-agenda'] = 'backend/agenda';
$route['backend/add-lingkungan'] = 'backend/lingkungan/view_form';
$route['backend/lingkungan/post'] = 'backend/lingkungan/proses';
$route['backend/lingkungan/post_ketua'] = 'backend/lingkungan/proses_ketua';
$route['backend/edit-lingkungan/(:num)'] = 'backend/lingkungan/view_form';
$route['backend/edit-ketua-lingkungan/(:num)'] = 'backend/lingkungan/view_form_ketua';
//list_user
$route['backend/user'] = 'backend/user';
$route['backend/user/update-user/(:num)'] = 'backend/user/view_form';
$route['backend/user/update-member/(:num)'] = 'backend/user/view_form';
$route['backend/user/proses/user'] = 'backend/user/proses';
$route['backend/user/detail/(:num)'] = 'backend/user/view_detail/$1';

$route['backend/user/proses_hapus_user'] = 'backend/user/proses_hapus_data';

$route['backend/user-akses'] = 'backend/user/akses';
$route['backend/user/add-user'] = 'backend/user/view_form';
$route['backend/user/edit/(:num)'] = 'backend/user/view_form';

$route['backend/member'] = 'backend/user';
$route['backend/member/edit/(:num)'] = 'backend/user/view_form';
$route['backend/member/add-member'] = 'backend/user/view_form';
$route['backend/member/detail/(:num)'] = 'backend/user/view_detail/$1';
$route['backend/user/proses/member'] = 'backend/user/proses';
$route['backend/user/proses_hapus_member'] = 'backend/user/proses_hapus_data';

$route['backend/pengajar'] = 'backend/user';
$route['backend/pengajar/add-pengajar'] = 'backend/user/view_form';
$route['backend/pengajar/edit/(:num)'] = 'backend/user/view_form';
$route['backend/pengajar/detail/(:num)'] = 'backend/user/view_detail/$1';
$route['backend/user/proses/pengajar'] = 'backend/user/proses';
$route['backend/user/proses_hapus_pengajar'] = 'backend/user/proses_hapus_data';

//absen
$route['backend/absen'] = 'backend/absen';
$route['backend/absen/add'] = 'backend/absen/view_form';
$route['backend/absen-get-kelas'] ='backend/absen/get_kelas';
$route['backend/absen/proses'] = 'backend/absen/proses';
$route['backend/absen/detail/(:num)/(:num)'] = 'backend/absen/detail';




//articles
$route['backend/articles'] = 'backend/articles';
$route['backend/article-add'] = 'backend/articles/add_article';

//instansi
$route['backend/instansi'] = 'backend/instansi';
$route['backend/instansi/add'] = 'backend/instansi/view_form';
$route['backend/instansi/edit/(:num)'] = 'backend/instansi/view_form';
$route['backend/instansi/proses'] = 'backend/instansi/proses';
$route['backend/instansi/block_instansi'] = 'backend/instansi/proses_block_instansi';

//kelas
$route['backend/kelas'] = 'backend/kelas';
$route['backend/kelas/add'] = 'backend/kelas/view_form';
$route['backend/kelas/edit/(:num)'] = 'backend/kelas/view_form';
$route['backend/kelas/proses'] = 'backend/kelas/proses';
$route['backend/kelas/block_kelas'] = 'backend/kelas/proses_block_kelas';

//laporan
$route['backend/laporan'] = 'backend/laporan';
$route['backend/cetak'] = 'backend/laporan/cetak';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
