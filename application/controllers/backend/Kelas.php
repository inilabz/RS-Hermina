<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('f_general');
        $this->load->model('m_security');
        $this->load->helper('backend_general');
        $this->load->library('user_agent');
        $this->m_security->getsecurity();
    }

    public function index() {
        $GetUserLogin = GetUserLogin();        
        $item = array(            
            'item' => $this->f_general->select('tbl_kelas','*','','','id_kelas DESC')            
        );

        $merge = array_merge($GetUserLogin, $item);
        $data = array(
            'content' => $this->load->view('backend/kelas/v_index', $merge, true),
            'css' => gen_css(array(base_url() . 'assets/plugins/sweetalert/sweetalert.css')),
            'script' => gen_script(
                array(
                    base_url() . 'assets/plugins/sweetalert/sweetalert-dev.js',
                    base_url() . 'assets/plugins/datatables.net/js/jquery.dataTables.min.js',
                    base_url() . 'assets/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js'
                ))
        );
        $this->load->view('backend/index', $data);
    }

    public function view_form() {
        $GetUserLogin = GetUserLogin();
        $merge = $GetUserLogin;
        if ($this->uri->segment(3) == 'edit') {
            $id = $this->uri->segment(4);
            $item = array('data_edit' => $this->f_general->select('tbl_kelas','*', array('id_kelas' => $id), 'row'));            
            $merge = array_merge($GetUserLogin, $item);
        }

        $data = array(
            'content' => $this->load->view('backend/kelas/v_form', $merge, true),
            'css' => gen_css(array(base_url() . 'assets/plugins/sweetalert/sweetalert.css')),
            'script' => gen_script(array(base_url() . 'assets/plugins/sweetalert/sweetalert-dev.js'))
        );
        $this->load->view('backend/index', $data);
    }

    public function proses() {    	
        $this->_set_rules_validation_form();
        if ($this->form_validation->run() === false) {
            $data = array('success' => 'kosong', 'msg' => array());
            foreach ($_POST as $key => $value) {
                $data['msg'][$key] = form_error($key);
            }
            echo json_encode($data);
            return;
        } else {
            $nama = $this->input->post('txtnama');      
            $durasi_kelas = $this->input->post('txtdurasi');    
            $id_kelas = $this->input->post('id_kelas');            

            $data = array(
                'nama_kelas' => $nama,
                'durasi_kelas' => $durasi_kelas,
                'status' => 1
            );
            if (empty($id_kelas)) {
                $data_merge = array_merge($data, array(
                    'created_by' => $this->session->userdata('admin_login'),
                    'created_at' => date('Y-m-d h:i:s')));
                $this->f_general->InsertOrUpdate('tbl_kelas', $data_merge);
            } else {
                $data_merge = array_merge($data, array(
                    'update_by' => $this->session->userdata('admin_login'),
                    'update_at' => date('Y-m-d h:i:s'))
                );
                $this->f_general->InsertOrUpdate('tbl_kelas', $data_merge, array('id_kelas' => $id_kelas));
            }

            echo json_encode(array('success' => true, 'msg' => array(
                    'title' => 'Success',
                    'type' => 'success',
                    'text' => 'Data Berhasil disimpan',
                    'link' => base_url('backend/instansi')
            )));
        }
    }

    public function proses_block_kelas() {
        $id_kelas = $this->input->post('id');
        $data_block = $this->input->post('data_block');
        if ($data_block > 0) {
            $delete_by = 0;
            $delete_at = '0000-00-00 00:00:00';
            $status = 0;
        } else {
            $delete_by = $this->session->userdata('admin_login');
            $delete_at = date('Y-m-d h:i:s');
            $status = 1;
        }
        $data = array(
            'delete_by' => $delete_by,
            'delete_at' => $delete_at,
            'status' => $status
        );
        $this->f_general->InsertOrUpdate('tbl_kelas', $data, array('id_kelas' => $id_kelas));
        echo json_encode($data);
    }

    private function _set_rules_validation_form() {
        $this->form_validation->set_rules('txtnama', 'Nama Kelas', 'trim|required', array('required' => '{field} Harus diisi'));
        $this->form_validation->set_rules('txtdurasi', 'Durasi Kelas', 'trim|required', array('required' => '{field} Harus diisi'));
        $this->form_validation->set_error_delimiters('<label class="error salah">', '</label>');
    }

}
