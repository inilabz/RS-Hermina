<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('f_general');
        $this->load->model('m_security');
        $this->load->helper('backend_general');
        $this->load->library('upload');
        $this->load->library('user_agent');
        $this->m_security->getsecurity();
    }

    public function index() {
        $GetUserLogin = GetUserLogin();
        if ($this->uri->segment(2) == 'user') {
            $button_add = 'backend/user/add-user';
            $button_update = 'backend/user/update-user/';
            $user_status_login = 2; //admin
        } elseif ($this->uri->segment(2) == 'pengajar') {
            $button_add = 'backend/pengajar/add-pengajar';
            $button_update = 'backend/pengajar/update-pengajar/';
            $user_status_login = 3; //member
        } else {
            $button_add = 'backend/member/add-member';
            $button_update = 'backend/user/update-user/';
            $user_status_login = 1; //member
        }

        $item = array(
            'item' => $this->f_general->select('tbl_user','*',array('user_status_login' => $user_status_login)),            
            'button_add' => $button_add,
            'button_update' => $button_update,
        );     
        $merge = array_merge($GetUserLogin, $item);
        $data = array(
            'content' => $this->load->view('backend/user/v_index', $merge, true),
            'css' => gen_css(
                array(
                    base_url() . 'assets/plugins/sweetalert/sweetalert.css'                                    
            )), 
            'script' => gen_script(
                array(
                    base_url() . 'assets/plugins/sweetalert/sweetalert-dev.js',
                    base_url() . 'assets/plugins/datatables.net/js/jquery.dataTables.min.js',
                    base_url() . 'assets/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js'
                ))
        );
        $this->load->view('backend/index', $data);
    }

	public function akses() {
		$GetUserLogin = GetUserLogin();
        $button_add = 'backend/user/add-user';
		$button_update = 'backend/user/update-user/';
		$url_proses = 'backend/user/proses/user';

        $items = array(
            'item' => $this->f_general->select('tbl_user_access','*'),
            'button_add' => $button_add,
            'button_update' => $button_update,
        );
        $merge = array_merge($GetUserLogin, $items);
        $data = array(
            'content' => $this->load->view('backend/user/v_index', $merge, true),
            'css' => gen_css(
                    array(
                        base_url() . 'assets/plugins/sweetalert/sweetalert.css',
                        base_url() . 'assets/backend/css/button_upload.css',
                        base_url() . 'assets/plugins/datepicker/datepicker.css'
                    )
            ),
            'script' => gen_script(
                    array(
                        base_url() . 'assets/plugins/sweetalert/sweetalert-dev.js',
                        base_url() . 'assets/plugins/datepicker/datepicker.js',
                    )
            )
        );
        $this->load->view('backend/index', $data);
    }

    public function view_form() {
        $GetUserLogin = GetUserLogin();
        if ($this->uri->segment(2) == 'user') {
            $url_proses = 'backend/user/proses/user';
        } else {
            $url_proses = 'backend/user/proses/member';
        }

        $items = array(
            'url_proses' => $url_proses,
            'instansi' => $this->f_general->select('tbl_instansi','*',array('status' => 1)),
            'kelas' => $this->f_general->select('tbl_kelas','*',array('status' => 1))
           );
        $merge = array_merge($GetUserLogin, $items);
        if ($this->uri->segment(3) == 'edit') {
            $id = $this->uri->segment(4);
            $data_kelas = $this->f_general->select('tbl_related','id_kelas',array('user_id' => $id)) ;
            $ary = [];
            foreach ($data_kelas as $keys) {
                $ary[] = $keys->id_kelas;
            }
            $data_edit = array(
                'data_edit' => $this->f_general->select('tbl_user','*',array('user_id' => $id), 'row'),
                'data_edit_kelas' => $ary 
            );        
          

            $merge = array_merge($merge, $data_edit);
        }

        $data = array(
            'content' => $this->load->view('backend/user/v_form', $merge, true),
            'css' => gen_css(
                    array(
                        base_url() . 'assets/plugins/sweetalert/sweetalert.css',
                        base_url() . 'assets/backend/css/button_upload.css',
                        base_url() . 'assets/plugins/datepicker/datepicker.css'
                    )
            ),
            'script' => gen_script(
                    array(
                        base_url() . 'assets/plugins/sweetalert/sweetalert-dev.js',
                        base_url() . 'assets/plugins/datepicker/datepicker.js',
                        base_url() . 'assets/plugins/select2/select2.min.js',
                    )
            )
        );
        $this->load->view('backend/index', $data);
    }

    public function proses() {        

        $user_id = $this->input->post('user_id');
        if (empty($user_id)) {
            $this->_set_rules_validation_form('insert');
        } else {
            $this->_set_rules_validation_form('update');
        }

        if ($this->form_validation->run() === false) {
            $data = array('success' => 'kosong', 'msg' => array());
            foreach ($_POST as $key => $value) {
                $data['msg'][$key] = form_error($key);
            }            

            if (!isset($_POST['user_kelas'])) {              
                $data['msg']['user_kelas'] = '<label class="error salah">Kelas Harus Diisi</label>';
            } 

            echo json_encode($data);
            return;
        } else {
            $url_full = base_url() . 'backend/user';

            $user_alamat = $this->input->post('user_alamat');
            $user_email = $this->input->post('user_email');
            $user_gender = $this->input->post('user_gender');
            $user_lingkungan = $this->input->post('user_lingkungan');
            $user_nama_lengkap = $this->input->post('user_nama_lengkap');
            $user_password = md5($this->input->post('user_password'));
            $user_phone = $this->input->post('user_phone');
            $user_phone_rumah = $this->input->post('user_phone_rumah');
            $user_status_pekerjaan = $this->input->post('user_status_pekerjaan');
            $user_tanggal_lahir = $this->input->post('user_tanggal_lahir');
            $user_status_login = $this->input->post('user_akses');
            $user_nik = $this->input->post('user_nik');
            $user_instansi = $this->input->post('user_instansi');
            $kelas = $this->input->post('user_kelas');



            $data = array(
                'user_nama' => $user_nama_lengkap,
                'user_email' => $user_email,
                'user_phone' => $user_phone,
                'user_phone_rumah' => $user_phone_rumah,
                'user_gender' => $user_gender,
                'user_alamat' => $user_alamat,
                'user_tanggal_lahir' => $user_tanggal_lahir,
                'user_status_login' => $user_status_login,
                'user_nik' => $user_nik,
                'id_instansi' => $user_instansi             
            );
            if (empty($user_id)) {
                $data_merge = array_merge($data, array(
                    'created_by' => $this->session->userdata('admin_login'),
                    'created_at' => date('Y-m-d h:i:s'),
                    'user_status' => 2,
                    'user_password' => $user_password)
                );

                if (!empty($_FILES['foto']['name'])) {
                    $gambar = gen_upload_image('foto', './assets/frontend/photo_profile/', './assets/frontend/photo_profile/resize/');
                    if ($gambar[1] == 'no') {
                        echo json_encode(array('success' => false, 'msg' => array(
                                'title' => 'Gagal',
                                'type' => 'error',
                                'text' => $gambar[0],
                                'link' => ''
                        )));
                        return;
                    } else {
                        $photo = array('user_photo' => $gambar[0]);
                        $data_merge = array_merge($data_merge, $photo);
                    }
                }

                $last_id = $this->f_general->InsertOrUpdate('tbl_user', $data_merge);

                for ($i=0; $i <count($kelas) ; $i++) { 
                    $this->f_general->InsertOrUpdate('tbl_related',['user_id'=>$last_id,'id_kelas'=>$kelas[$i]]);
                }
            } else {
                $this->db->delete('tbl_related', array('user_id' => $user_id));
                for ($i=0; $i <count($kelas) ; $i++) { 
                    $this->f_general->InsertOrUpdate('tbl_related',['user_id'=>$user_id,'id_kelas'=>$kelas[$i]]);
                }
                if ($this->input->post('user_password') != '') {
                    $data_merge = array_merge($data, array(
                        'update_by' => $this->session->userdata('admin_login'),
                        'user_password' => $user_password,
                        'update_at' => date('Y-m-d h:i:s'))
                    );
                } else {
                    $data_merge = array_merge($data, array(
                        'update_by' => $this->session->userdata('admin_login'),
                        'update_at' => date('Y-m-d h:i:s'))
                    );
                }
                if (!empty($_FILES['foto']['name'])) {
                    $gambar = gen_upload_image('foto', './assets/frontend/photo_profile/', './assets/frontend/photo_profile/resize/');
                    if ($gambar[1] == 'no') {
                        echo json_encode(array('success' => false, 'msg' => array(
                                'title' => 'Gagal',
                                'type' => 'error',
                                'text' => $gambar[0],
                                'link' => ''
                        )));
                        return;
                    } else {
                        $photo = array('user_photo' => $gambar[0]);
                        $data_merge = array_merge($data_merge, $photo);
                    }
                }
                $this->f_general->InsertOrUpdate('tbl_user', $data_merge, array('user_id' => $user_id));
            }

            echo json_encode(array('success' => true, 'msg' => array(
                    'title' => 'Success',
                    'type' => 'success',
                    'text' => 'Data Berhasil disimpan',
                    'link' => $url_full
            )));
        }
    }

    public function proses_hapus_data() {
        $user_id = $this->input->post('id');
        $data_block = $this->input->post('data_block');
        if ($data_block > 0) {
            $delete_by = 0;
            $delete_at = '0000-00-00 00:00:00';
        } else {
            $delete_by = $this->session->userdata('admin_login');
            $delete_at = date('Y-m-d h:i:s');
        }
        $data = array(
            'delete_by' => $delete_by,
            'delete_at' => $delete_at);
        $this->f_general->InsertOrUpdate('tbl_user', $data, array('user_id' => $user_id));
        echo json_encode($data);
    }

    private function _set_rules_validation_form($param) {
        if ($param == 'insert') {
            $this->form_validation->set_rules('user_nama_lengkap', 'Nama Lengkap', 'trim|required', array('required' => '{field} Harus diisi'));
            $this->form_validation->set_rules('user_akses', 'User Akses', 'trim|required', array('required' => '{field} Harus diisi'));
            $this->form_validation->set_rules('user_password', 'Password', 'trim|required', array('required' => '{field} Harus diisi'));
            $this->form_validation->set_rules('user_gender', 'Jenis Kelamin', 'trim|required', array('required' => '{field} Harus diisi'));
            $this->form_validation->set_rules('user_nik', 'NIK', 'trim|required', array('required' => '{field} Harus diisi'));
            $this->form_validation->set_rules('user_email', 'Email', 'required|callback_email_check');
            $this->form_validation->set_rules('user_instansi', 'Instansi', 'trim|required', array('required' => '{field} Harus diisi'));                       
        } else {
            $this->form_validation->set_rules('user_nama_lengkap', 'Nama Lengkap', 'trim|required', array('required' => '{field} Harus diisi'));
            $this->form_validation->set_rules('user_akses', 'User Akses', 'trim|required', array('required' => '{field} Harus diisi'));
            $this->form_validation->set_rules('user_gender', 'Jenis Kelamin', 'trim|required', array('required' => '{field} Harus diisi'));
            $this->form_validation->set_rules('user_nik', 'NIK', 'trim|required', array('required' => '{field} Harus diisi'));
            $this->form_validation->set_rules('user_email', 'Email', 'required');
            $this->form_validation->set_rules('user_instansi', 'Instansi', 'trim|required', array('required' => '{field} Harus diisi'));            
        }

        $this->form_validation->set_error_delimiters('<label class="error salah">', '</label>');
    }

    public function email_check($email) {
        if ($email == '' || empty($email)) {
            $this->form_validation->set_message('email_check', '{field} Harus Di Isi');
            return FALSE;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->form_validation->set_message('email_check', '{field} Tidak Valid');
            return FALSE;
        }

        $check = $this->f_general->select('tbl_user','*',array('user_email' => $this->input->post('user_email')), 'row');
        if ($check) {
            $this->form_validation->set_message('email_check', 'Maaf,{field} Sudah Terdaftar');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function view_detail() {
        $GetUserLogin = GetUserLogin();
        $user_id = $this->uri->segment(4);    
        $result = ['detail' =>$this->f_general->select('tbl_user','*',array('user_id' => $user_id), 'row')];     
        $merge = array_merge($GetUserLogin, $result);        
        $data = array(
            'content' => $this->load->view('backend/user/v_detail', $merge, true),
        );
        $this->load->view('backend/index', $data);
    }

}
