<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('f_general');
        $this->load->model('m_security');
        $this->load->helper('backend_general');
        $this->load->library('user_agent');
        $this->m_security->getsecurity();
    }

    public function index() {
        $GetUserLogin = GetUserLogin();        
        $id_session = $this->session->userdata('admin_login');
        $from = "tbl_related_absen a";
        $select_fields = 'a.id_absen,a.id_karyawan,a.id_pengajar,a.keterangan,b.id_kelas,c.nama_kelas,sum(c.durasi_kelas) as total_hadir,d.user_nama';
        $join = array(
            array(
                "fields_1" => "tbl_absen b",
                "fields_2" => "a.id_absen = b.id_absen",
                "fields_3" => "left"
                ),
            array(
                "fields_1" => "tbl_kelas c",
                "fields_2" => "b.id_kelas = c.id_kelas",
                "fields_3" => "left"
                ),
            array(
                "fields_1" => "tbl_user d",
                "fields_2" => "a.id_karyawan = d.user_id",
                "fields_3" => "left"
                )
            );
        $group_by = 'a.id_karyawan ';
        $order_by = 'total_hadir desc';
        $where = array('a.keterangan'=>'M');
        $data = $this->f_general->join_tabel($from,$select_fields,$join,$where,'','',$group_by,$order_by);
     
        $item = array(            
            'item' => $data
        );             
        

        $merge = array_merge($GetUserLogin, $item);
        $data = array(
            'content' => $this->load->view('backend/laporan/v_index', $merge, true),
            'css' => gen_css(array(base_url() . 'assets/plugins/sweetalert/sweetalert.css')),
            'script' => gen_script(
                array(
                    base_url() . 'assets/plugins/sweetalert/sweetalert-dev.js',
                    base_url() . 'assets/plugins/datatables.net/js/jquery.dataTables.min.js',
                    base_url() . 'assets/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js'
                ))
        );
        $this->load->view('backend/index', $data);
    }

    public function view_form() {
        $GetUserLogin = GetUserLogin();
        $merge = $GetUserLogin;
        if ($this->uri->segment(3) == 'edit') {
            $id = $this->uri->segment(4);
            $item = array('data_edit' => $this->f_general->select('tbl_kelas','*', array('id_kelas' => $id), 'row'));            
            $merge = array_merge($GetUserLogin, $item);
        }

        $data = array(
            'content' => $this->load->view('backend/kelas/v_form', $merge, true),
            'css' => gen_css(array(base_url() . 'assets/plugins/sweetalert/sweetalert.css')),
            'script' => gen_script(array(base_url() . 'assets/plugins/sweetalert/sweetalert-dev.js'))
        );
        $this->load->view('backend/index', $data);
    }

    public function cetak(){
    	header("Content-type: application/octet-stream");

		header("Content-Disposition: attachment; filename=lapran.xls");

		header("Pragma: no-cache");

		header("Expires: 0");
		$html = '';

		$from = "tbl_related_absen a";
        $select_fields = 'a.id_absen,a.id_karyawan,a.id_pengajar,a.keterangan,b.id_kelas,c.nama_kelas,sum(c.durasi_kelas) as total_hadir,d.user_nama';
        $join = array(
            array(
                "fields_1" => "tbl_absen b",
                "fields_2" => "a.id_absen = b.id_absen",
                "fields_3" => "left"
                ),
            array(
                "fields_1" => "tbl_kelas c",
                "fields_2" => "b.id_kelas = c.id_kelas",
                "fields_3" => "left"
                ),
            array(
                "fields_1" => "tbl_user d",
                "fields_2" => "a.id_karyawan = d.user_id",
                "fields_3" => "left"
                )
            );
        $group_by = 'a.id_karyawan ';
        $order_by = 'total_hadir desc';
        $where = array('a.keterangan'=>'M');
        $data = $this->f_general->join_tabel($from,$select_fields,$join,$where,'','',$group_by,$order_by);

        $html .= '<table border="1" width="100%">';
        $html .= '<thead>';
        $html .= '<tr>';
        $html .= '<td>NO</td>';        
        $html .= '<td>Nama Karyawan</td>';
        $html .= '<td>Total Durasi</td>';
        $html .= '</tr>';
        $html .='</thead><tbody>';
		$no = 1;
        foreach ($data as $key) {                                                                    
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$key->user_nama.'</td>';                                    
            $html .= '<td>'.$key->total_hadir.' Menit</td>';            
            $html .= '</tr>';
        }
        $html .= '<tbody></table>';

        echo $html;


    }

}
