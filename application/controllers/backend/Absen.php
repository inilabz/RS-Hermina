<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Absen extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('f_general');
        $this->load->model('m_security');
        $this->load->helper('backend_general');
        $this->load->library('user_agent');
        $this->m_security->getsecurity();
    }

    public function index() {
        $GetUserLogin = GetUserLogin();        
        $id_session = $this->session->userdata('admin_login');
        $from = "tbl_related_absen a";
        $select_fields = 'a.id_absen,b.tanggal_absen,c.nama_kelas,count(a.id_karyawan) as total_hadir,c.id_kelas';
        $join = array(
            array(
                "fields_1" => "tbl_absen b",
                "fields_2" => "a.id_absen = b.id_absen",
                "fields_3" => "left"
                ),
            array(
                "fields_1" => "tbl_kelas c",
                "fields_2" => "b.id_kelas = c.id_kelas",
                "fields_3" => "left"
                )
            );
        $group_by = 'a.id_absen ';
        $order_by = 'a.id_absen desc';
        $where = array('b.status_absen'=>1,'id_pengajar' => $id_session);
        $data = $this->f_general->join_tabel($from,$select_fields,$join,$where,'','',$group_by,$order_by);
     
        $item = array(            
            'item' => $data
        );

        

        

        $merge = array_merge($GetUserLogin, $item);
        $data = array(
            'content' => $this->load->view('backend/absen/v_index', $merge, true),
            'css' => gen_css(array(base_url() . 'assets/plugins/sweetalert/sweetalert.css')),
            'script' => gen_script(
                array(
                    base_url() . 'assets/plugins/sweetalert/sweetalert-dev.js',
                    base_url() . 'assets/plugins/datatables.net/js/jquery.dataTables.min.js',
                    base_url() . 'assets/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js'
                ))
        );
        $this->load->view('backend/index', $data);
    }

    public function view_form() {
        $GetUserLogin = GetUserLogin();
        $merge = $GetUserLogin;
        $item = array(
            'kelas' =>$this->f_general->select('tbl_kelas','*', array('status' => 1)),
            'name_token' => $this->security->get_csrf_token_name(),
            'value_token' => $this->security->get_csrf_hash()
        );        
        $merge = array_merge($GetUserLogin, $item);        

        $data = array(
            'content' => $this->load->view('backend/absen/v_form', $merge, true),
            'css' => gen_css(
                    array(
                        base_url() . 'assets/plugins/sweetalert/sweetalert.css',
                        base_url() . 'assets/plugins/datepicker/datepicker.css'
                    )
                ),
            'script' => gen_script(
                    array(
                        base_url() . 'assets/plugins/sweetalert/sweetalert-dev.js',
                        base_url() . 'assets/plugins/datepicker/datepicker.js',
                    )
                )
        );
        $this->load->view('backend/index', $data);
    }

    // public function proses() {    	
    //     $this->_set_rules_validation_form();
    //     if ($this->form_validation->run() === false) {
    //         $data = array('success' => 'kosong', 'msg' => array());
    //         foreach ($_POST as $key => $value) {
    //             $data['msg'][$key] = form_error($key);
    //         }
    //         echo json_encode($data);
    //         return;
    //     } else {
    //         $nama = $this->input->post('txtnama');      
    //         $durasi_kelas = $this->input->post('txtdurasi');    
    //         $id_kelas = $this->input->post('id_kelas');            

    //         $data = array(
    //             'nama_kelas' => $nama,
    //             'durasi_kelas' => $durasi_kelas,
    //             'status' => 1
    //         );
    //         if (empty($id_kelas)) {
    //             $data_merge = array_merge($data, array(
    //                 'created_by' => $this->session->userdata('admin_login'),
    //                 'created_at' => date('Y-m-d h:i:s')));
    //             $this->f_general->InsertOrUpdate('tbl_kelas', $data_merge);
    //         } else {
    //             $data_merge = array_merge($data, array(
    //                 'update_by' => $this->session->userdata('admin_login'),
    //                 'update_at' => date('Y-m-d h:i:s'))
    //             );
    //             $this->f_general->InsertOrUpdate('tbl_kelas', $data_merge, array('id_kelas' => $id_kelas));
    //         }

    //         echo json_encode(array('success' => true, 'msg' => array(
    //                 'title' => 'Success',
    //                 'type' => 'success',
    //                 'text' => 'Data Berhasil disimpan',
    //                 'link' => base_url('backend/instansi')
    //         )));
    //     }
    // }

    public function proses_block_kelas() {
        $id_kelas = $this->input->post('id');
        $data_block = $this->input->post('data_block');
        if ($data_block > 0) {
            $delete_by = 0;
            $delete_at = '0000-00-00 00:00:00';
            $status = 0;
        } else {
            $delete_by = $this->session->userdata('admin_login');
            $delete_at = date('Y-m-d h:i:s');
            $status = 1;
        }
        $data = array(
            'delete_by' => $delete_by,
            'delete_at' => $delete_at,
            'status' => $status
        );
        $this->f_general->InsertOrUpdate('tbl_kelas', $data, array('id_kelas' => $id_kelas));
        echo json_encode($data);
    }

    private function _set_rules_validation_form() {
        $this->form_validation->set_rules('txtnama', 'Nama Kelas', 'trim|required', array('required' => '{field} Harus diisi'));
        $this->form_validation->set_rules('txtdurasi', 'Durasi Kelas', 'trim|required', array('required' => '{field} Harus diisi'));
        $this->form_validation->set_error_delimiters('<label class="error salah">', '</label>');
    }

    public function get_kelas(){
        $id_kelas = $this->input->post('id_kelas');        
        $data = $this->get_related($id_kelas,1);            
        $item = array(            
            'data_karyawan' =>['data'=>$data,'total'=>count($data)]
        );
        echo json_encode($item);
    }

    public function get_related($id_kelas,$status_pengajar){
        $from = "tbl_related a";
        $select_fields = 'a.user_id,a.id_kelas,b.nama_kelas,b.durasi_kelas,c.user_nama,c.user_email,c.user_status';
        $join = array(
            array(
                "fields_1" => "tbl_kelas b",
                "fields_2" => "a.id_kelas = b.id_kelas",
                "fields_3" => "left"
                ),
            array(
                "fields_1" => "tbl_user c",
                "fields_2" => "a.user_id = c.user_id",
                "fields_3" => "left"
                )
            );
        $where = array('a.id_kelas'=>$id_kelas,'c.user_status_login'=>$status_pengajar,'c.user_status'=>2);
        $data = $this->f_general->join_tabel($from,$select_fields,$join,$where);
        return $data;
    }

    public function proses(){        
        $id_session = $this->session->userdata('admin_login');
        $data = array(
            'id_kelas' => $this->input->post('txtkelas'),
            'tanggal_absen' => date('Y-m-d h:i:s'),
            'status_absen' => 1,
            'created_by' => $this->session->userdata('admin_login'),
            'created_at' => date('Y-m-d h:i:s'),
        );
        $last_id = $this->f_general->InsertOrUpdate('tbl_absen', $data);

        $related_absen = array();
        foreach ($_POST as $key => $value) {
            if($key == 'txtkelas'){
                continue;
            }            
            $related_absen[] = ['id_absen'=>$last_id,'id_karyawan'=>$key,'id_pengajar'=>$id_session,'keterangan'=>$value,'date_add'=>date('Y-m-d h:i:s')];
        }        
        $this->db->insert_batch('tbl_related_absen', $related_absen);
        redirect(base_url().'backend/absen/detail/'.$last_id.'/'.$this->input->post('txtkelas'));
        
    }

    public function detail(){
        $id_absen = $this->uri->segment(4);
        $id_kelas = $this->uri->segment(5);
        $GetUserLogin = GetUserLogin();      

        $from = "tbl_related_absen a";
        $select_fields = 'a.id_absen,a.id_karyawan,a.keterangan,b.id_kelas,c.user_nama,c.user_email,c.user_status';
        $join = array(
            array(
                "fields_1" => "tbl_absen b",
                "fields_2" => "a.id_absen = b.id_absen",
                "fields_3" => "left"
                ),
            array(
                "fields_1" => "tbl_user c",
                "fields_2" => "a.id_karyawan = c.user_id",
                "fields_3" => "left"
                )
            );
        $where = array('a.id_absen'=>$id_absen);
        $item = [
            'data_karyawan'=>$this->f_general->join_tabel($from,$select_fields,$join,$where),
            'kelas' =>$this->f_general->select('tbl_kelas','*', array('id_kelas' => $id_kelas),'row')
        ];        
        

        $merge = array_merge($GetUserLogin, $item);
        $data = array(
            'content' => $this->load->view('backend/absen/v_detail', $merge, true),
            'css' => gen_css(array(base_url() . 'assets/plugins/sweetalert/sweetalert.css')),
            'script' => gen_script(
                array(
                    base_url() . 'assets/plugins/sweetalert/sweetalert-dev.js',
                    base_url() . 'assets/plugins/datatables.net/js/jquery.dataTables.min.js',
                    base_url() . 'assets/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js'
                ))
        );
        $this->load->view('backend/index', $data);
        
    }

}
