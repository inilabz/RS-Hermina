<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

public function __construct() 
{
    parent::__construct(); 
    $this->load->model('f_general');   
    $this->load->model('m_security');
    $this->load->helper('backend_general');    
    $this->m_security->getsecurity();       	        				  
}

public function index()
{        
    $GetUserLogin = GetUserLogin(); 

    $data = array(
        'content' => $this->load->view('backend/home/v_home',$GetUserLogin,true),
    );        
    $this->load->view('backend/index',$data);
}




}
