<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('f_general');
    }

    public function form_login() {
        $this->load->view('backend/login/v_login');
    }

    public function proses_login() {
        $this->_set_rules_validation_login();
        if ($this->form_validation->run() === false) {
            $data = array('success' => 'kosong', 'msg' => array());
            foreach ($_POST as $key => $value) {
                $data['msg'][$key] = form_error($key);
            }
            echo json_encode($data);
            return;
        } else {
            $email = $this->input->post('email');
            $password = md5($this->input->post('password'));            
            $sql_cek_login = $this->f_general->select('tbl_user','*',array('user_email' => $email, 'user_password' => $password, 'user_status_login' => 2, 'user_status' => 2),'row');

            if (count($sql_cek_login) > 0) {
                $result_data = array('success' => true, 'msg' => array('title' => 'Sukses', 'type' => 'success', 'text' => 'Data yang anda masuka benar', 'link' => 'backend/dashboard'));
                $session = array('admin_login' => $sql_cek_login->user_id);
                $this->session->set_userdata($session);
            } else {
                $result_data = array('success' => 'gagal', 'msg' => array('title' => 'Oops...', 'type' => 'error', 'text' => 'Maaf, Email atau Password Anda Salah !'));
            }
            echo json_encode($result_data);
            return;
        }
    }

    private function _set_rules_validation_login() {
        $this->form_validation->set_rules('email', 'Email', 'callback_email_check');
        $this->form_validation->set_rules('password', 'Password', 'trim|required', array('required' => '{field} Harus Diisi'));
        $this->form_validation->set_error_delimiters('<label class="error salah">', '</label>');
    }

    public function email_check($email) {
        if ($email == '' || empty($email)) {
            $this->form_validation->set_message('email_check', '{field} Harus DiIsi');
            return FALSE;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->form_validation->set_message('email_check', '{field} Tidak Valid');
            return FALSE;
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url() . 'backend/security/login', 'refresh');
    }

}
