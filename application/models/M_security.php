<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_security extends CI_model {		
 	public function getsecurity(){
		$id_user_login = $this->session->userdata('admin_login');
		if(empty($id_user_login)){
			$this->session->sess_destroy();
			redirect('backend/security/login');
		}
	}
}
