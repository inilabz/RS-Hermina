<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class F_general extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
  
    public function InsertOrUpdate($table, $data, $id = null) {
        if (!empty($id)) {
            $this->db->where($id);
            $this->db->update($table, $data);
            return;
        }
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

   public  function select($tabel, $fields, $where = null,$callback = null,$urutan = null,  $where_in = null, $like = null, $rowperpage = null, $rowno = null) {
       
        $db = $this->db;        

        $db->select($fields); //typenya string cth: 'title, content, date'

        if (!empty($where)) {
            $db->where($where); //type array cth: array('id' =>1)
        }

        if (!empty($where_in)) {
            $db->where_in($where_in[0], $where_in[1]); //type array cth: array('id' =>1)
        }

        if (!empty($like)) {
            $no = 0;
            foreach ($like as $key) {
                $no++;
                if ($no == 1) {
                    $db->like($key['fields_1'], $key['fields_2'], $key['fields_3']);
                    continue;
                }
                if ($no > 1) {
                    $db->or_like($key['fields_1'], $key['fields_2'], $key['fields_3']);
                }
            }
        }

        if (!empty($urutan)) {
            $db->order_by($urutan); //type string cth : 'id desc'
        }

        if ($rowperpage != '') {
            $db->limit($rowperpage, $rowno); // $rowperpage = int
        }

        if ($callback == 'row') {
            return $db->get($tabel)->row(); // $tabel = string
        } else if ($callback == 'num_rows') {
            return $db->get($tabel)->num_rows();
        } else if ($callback == 'result_array') {
            return $db->get($tabel)->result_array();
        } else {
            return $db->get($tabel)->result();
        }
    }

   public  function join_tabel($from, $select_fields,$join,$where = null,$callback = null,$like = null,$group_by = null,$order_by = null,$rowperpage = null,$rowno = null) {    
            $db = $this->db;       
        
        $db->select($select_fields);
        $db->from($from);
        
        foreach ($join as $key) {
            $db->join($key['fields_1'], $key['fields_2'], $key['fields_3']);
        }        
        
        if (!empty($where)) {
            $db->where($where);
        }
        
        if (!empty($like)) {
            $no = 0;
            foreach ($like as $key) {
                $no++;
                if ($no == 1) {
                    $db->like($key['fields_1'], $key['fields_2'], $key['fields_3']);
                    continue;
                }
                if ($no > 1) {
                    $db->or_like($key['fields_1'], $key['fields_2'], $key['fields_3']);
                }
            }
        }
        
        if (!empty($group_by)) {
            $db->group_by($group_by);
        }
        
        if (!empty($order_by)) {
            $db->order_by($order_by);
        }
                
        if (!empty($rowperpage)) {
            $db->limit($rowperpage, $rowno);
        }
        
        if ($callback == 'row') {
            return $db->get()->row();
        } else if ($callback == 'num_rows') {
            return $db->get()->num_rows();
        } else if ($callback == 'result_array') {
            return $db->get()->result_array();
        } else {
            return $db->get()->result();
        }
        
    }

}
