<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function GetUserLogin() {
    $ci = & get_instance();
    $session = $ci->session->userdata('admin_login');
    $fields = 'b.user_nama,b.user_email,a.user_id,a.user_access_id,c.user_access_id,c.user_access_name';
    $join = array(
        array(
            'fields_1' => 'tbl_user b',
            'fields_2' => 'b.user_id=a.user_id',
            'fields_3' => 'join',
        ),
        array(
            'fields_1' => 'tbl_user_access c',
            'fields_2' => 'c.user_access_id=a.user_access_id',
            'fields_3' => 'join',
        )
    );
    $where = array('a.user_id' => $session);
    $sql = $ci->f_general->join_tabel('tbl_user_group a',$fields, $join, $where,'row');
    $data_user_login = array(
        'user_login_nama' => $sql->user_nama,
        'user_access_name' => $sql->user_access_name
    );
    return $data_user_login;
}

function get_total_karyawan_per_kelas($id_kelas){
    $ci = & get_instance();
    $session = $ci->session->userdata('admin_login');
    $fields = 'count(a.user_id) as total_per_kelas';
    $join = array(
        array(
            'fields_1' => 'tbl_user b',
            'fields_2' => 'a.user_id=b.user_id',
            'fields_3' => 'join',
        )
    );
    $where = array('b.user_status_login' => 1,'a.id_kelas'=>$id_kelas);
    $sql = $ci->f_general->join_tabel('tbl_related  a',$fields, $join, $where,'row');
    return $sql;
}

function Breadcrumbs($section_id, $result = null, $index = null) {

    $ci = & get_instance();

    $data = $ci->f_general->select('tbl_section','*', array('section_id' => $section_id, 'status' => 1), 'row');
    if ($data->section_parent == 0) {
        $result[] = [$data->section_id, $data->section_nama];
        sort($result);
        $clength = count($result);
        $html = '';
        for ($x = 0; $x < $clength; $x++) {
            $html .= '<li class="breadcrumb-item"><a href="' . base_url() . 'backend/section/sub-menu/' . $result[$x][0] . '"><b>' . ucwords($result[$x][1]) . '</b></a></li> ';
        }
        echo $html;
    } else {
        $result[] = [$data->section_id, $data->section_nama];
        Breadcrumbs($data->section_parent, $result);
    }
}

function gen_script($data = array()) {
    $return = '';
    foreach ($data as $row) {
        $return .= '<script src="' . $row . '" type="text/javascript"></script>';
    }
    return $return;
}

function gen_css($data = array()) {
    $return = '';
    foreach ($data as $row) {
        $return .= '<link href="' . $row . '" rel="stylesheet">';
    }
    return $return;
}

function now_url() {
    // echo "<pre>";var_dump($_SERVER['QUERY_STRING']);exit;
    $ci = & get_instance();

    $url = $ci->config->site_url($ci->uri->uri_string());
    return $_SERVER['QUERY_STRING'] ? $url . '?' . $_SERVER['QUERY_STRING'] : $url;
}

function paging_tmp() {
    $data = array(
        'use_page_numbers' => true,
        'page_query_string' => true,
        'query_string_segment' => 'page',
        // 'display_pages' => FALSE,
        'full_tag_open' => '<div class="btn-group">',
        'full_tag_close' => '</div>',
        // 'first_tag_open' => '<button type="button" class="btn btn-default btn-sm">',
        // 'first_tag_close' => '</button>',
        // 'last_tag_open' => '<button type="button" class="btn btn-default btn-sm">',
        // 'last_tag_close' => '</button>',
        // 'next_tag_open' => '<button type="button" class="btn btn-default btn-sm">',
        // 'next_tag_close' => '</button>',
        // 'next_link' => '<i class="fa fa-chevron-right"></i>',
        // 'prev_tag_open' => '<button type="button" class="btn btn-default btn-sm">',
        // 'prev_tag_close' => '</button>',
        // 'prev_link' => '<i class="fa fa-chevron-left"></i>',
        'cur_tag_open' => '<button type="button" class="btn btn-primary btn-sm">',
        'cur_tag_close' => '</button>',
            // 'num_tag_open' => '<button type="button" class="btn btn-default btn-sm">',
            // 'num_tag_close' => '</button>'
    );
    return $data;
}

function gen_paging($total = 0, $limit = 10) {
    // echo $total. ' '.$limit;exit;
    $ci = & get_instance();
    $base_url = current_url();
    $base_url .= get_query_string('page');
    $config = paging_tmp();
    $config['base_url'] = $base_url;
    $config['total_rows'] = $total;
    $config['per_page'] = $limit;

    $ci->pagination->initialize($config);
    $data = $ci->pagination->create_links();
    return str_replace('<a href', '<a class="btn btn-default btn-sm" href', $data);
}

function get_query_string($remove = '') {
    $query_string = $_GET;
    if ($remove) {
        if (is_array($remove)) {
            foreach ($remove as $key => $value) {
                unset($query_string[$value]);
            }
        } else {
            unset($query_string[$remove]);
        }
    }
    if ($query_string) {
        return '?' . http_build_query($query_string);
    }
    return '';
}

function gen_total($total, $limit, $offset) {
    $min = $offset + 1;
    $max = $offset + $limit;
    if ($total < $limit) {
        $max = $total;
    }
    if ($total) {
        if ($min == $max) {
            return 'Showing ' . $min . ' of ' . $total . ' entries';
        } elseif ($max > $total) {
            return 'Showing last of ' . $total . ' entries';
        } else {
            return 'Showing ' . $min . ' to ' . $max . ' of ' . $total . ' entries';
        }
    }
    return 'Data is not found';
}

function gen_menu_headline() {
    $ci = & get_instance();

    $data_menu = array();
    $data_site = $ci->general_model->get('site', array('id', 'name', 'alias'), array('status' => 1))->result();
    foreach ($data_site as $index => $row) {
        $data_menu[$index]['id'] = $row->id;
        $data_menu[$index]['name'] = $row->name;
        $data_menu[$index]['alias'] = $row->alias;
        $data_menu[$index]['section'] = $ci->general_model->get('section', array('id', 'name', 'site_id'), array('status' => 1, 'site_id' => $row->id))->result();
    }

    $data['data_menu'] = $data_menu;

    return $ci->load->view('menu_headline', $data, true);
}

function gen_menu_berita_pilihan() {
    $ci = & get_instance();

    $data_menu = array();
    $data_site = $ci->general_model->get('site', array('id', 'name'), array('status' => 1))->result();

    foreach ($data_site as $index => $row) {
        $data_menu[$index]['id'] = $row->id;
        $data_menu[$index]['name'] = $row->name;
        $data_menu[$index]['section'] = $ci->general_model->get('section', array('id', 'name', 'site_id'), array('status' => 1, 'site_id', $row->id))->result();
    }

    $data['data_menu'] = $data_menu;

    return $ci->load->view('menu_berita_pilihan', $data, true);
}

function gen_access($data) {
    $return = '';
    $data = explode(',', $data);
    foreach ($data as $row) {
        $row = str_replace('/', ' -> ', $row);
        $return .= '<label class="label label-default">' . $row . '</label> ';
    }
    return $return;
}

function dd($data) {
    echo "<pre>";
    var_dump($data);
    exit;
}

function gen_page() {
    $ci = & get_instance();
    $page = 1;
    if ($ci->input->get('page')) {
        $page = $ci->input->get('page');
    }
    return $page;
}

function gen_limit($limit = 10) {
    $ci = & get_instance();
    if ($ci->input->get('limit')) {
        $limit = $ci->input->get('limit');
    }
    return $limit;
}

function gen_offset($page, $limit) {
    return ($page - 1) * $limit;
}

function format_dmy($date) {
    if (isset($date) && $date <> '0000-00-00 00:00:00' && $date <> null) {
        $date = date_create($date);
        $date = date_format($date, 'd-M-Y H:i:s');
        return $date;
    }
}

function get_iptc($imagepath) {
    $data = array();

    $size = @getimagesize($imagepath, $info);

    if (is_array($info)) {
        if (!isset($info["APP13"])) {
            return false;
        }

        $iptc = iptcparse($info["APP13"]);
        $datecreate = (isset($iptc['2#055'][0])) ? $iptc['2#055'][0] : "";
        $datecreate = (!empty($datecreate)) ? substr($datecreate, 0, 4) . '-' . substr($datecreate, 4, 2) . '-' . substr($datecreate, 6, 2) : "";
        $data = array(
            'NAME' => (isset($iptc['2#005'][0])) ? $iptc['2#005'][0] : "",
            'CREATED_AT' => $datecreate,
            'AUTHOR' => (isset($iptc['2#080'][0])) ? $iptc['2#080'][0] : "",
            'CITY' => (isset($iptc['2#090'][0])) ? $iptc['2#090'][0] : "",
            'PROVINCE' => (isset($iptc['2#095'][0])) ? $iptc['2#095'][0] : "",
            'CODECOUNTRY' => (isset($iptc['2#100'][0])) ? $iptc['2#100'][0] : "",
            'COUNTRY' => (isset($iptc['2#101'][0])) ? $iptc['2#101'][0] : "",
            'TITLE' => (isset($iptc['2#105'][0])) ? $iptc['2#105'][0] : "",
            'CAPTION' => (isset($iptc['2#120'][0])) ? $iptc['2#120'][0] : "",
            'CAPTION_WRITER' => (isset($iptc['2#122'][0])) ? $iptc['2#122'][0] : "",
            'SOURCE' => (isset($iptc['2#115'][0])) ? $iptc['2#115'][0] : "",
            'KEYWORDS' => (isset($iptc['2#025'][0])) ? $iptc['2#025'][0] : "",
            'CREDITS' => (isset($iptc['2#110'][0])) ? $iptc['2#110'][0] : "",
            'COPYRIGHT' => (isset($iptc['2#116'][0])) ? $iptc['2#116'][0] : ""
        );
    }

    return $data;
}

function gen_video_youtube($url) {
    $parts = parse_url($url);
    parse_str($parts['query'], $query);
    $v = $query['v'];
    $data = json_decode(file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=snippet&id=" . $v . "&key=AIzaSyD23_HTzzIxTGTTLhVwauUxZuZlu0sCfMY"));
    return $data;
}

function gen_menu($access) {
    $ci = & get_instance();
    $data['access'] = explode(',', $access);
    $data['site'] = $ci->general_model->get('site', null)->result();
    return $ci->load->view('menu_view', $data, true);
}

function gen_menu_article() {
    $ci = & get_instance();
    $data['site'] = $ci->general_model->get('site', null)->result();
    return $ci->load->view('menu_article', $data, true);
}

function gen_menu_gallery() {
    $ci = & get_instance();
    $user_login = $ci->session->userdata('user_login');
    $data['access'] = explode(',', $user_login['access']);
    $data['site'] = $ci->general_model->get('site', null)->result();
    return $ci->load->view('menu_gallery', $data, true);
}

function get_thumbor_thumb($url = '', $width = 100, $height = 100, $aligment = '', $crop = '', $watermark = '') {
    if (strpos($url, 'asset-a.grid.id') !== false) {
        $patch = explode('/', $url);
        $pos = strpos($url, 'filters:');
        if (!$pos) {
            $pos = strpos($url, 'photo/');
        }

        $back = substr($url, $pos);
        if ($aligment) {
            $back = $aligment . '/' . $back;
        }
        if ($watermark) {
            $back = 'filters:watermark(' . config_item('img_watermark') . ',0,0,0)/' . $back;
        }
        if ($patch[3] == 'crop') {
            $return = config_item('assets_s3') . 'crop/' . $patch[4] . '/' . $width . 'x' . $height . '/' . $back;
        } else {
            if ($crop) {
                $datacrop = explode(':', $crop);
                $koordinat = explode('x', $datacrop[0]);
                $size = explode('x', $datacrop[1]);
                $return = config_item('assets_s3') . 'crop/' . $koordinat[0] . 'x' . $koordinat[1] . ':' . ($koordinat[0] + $size[0]) . 'x' . ($koordinat[1] + $size[1]) . '/' . $width . 'x' . $height . '/' . $back;
            } else {
                $return = config_item('assets_s3') . 'crop/0x0:0x0/' . $width . 'x' . $height . '/' . $back;
            }
        }
        return $return;
    } else {
        return $url;
    }
}

function get_user($user_id) {
    $ci = & get_instance();
    $result = $ci->general_model->get('user', null, 'id="' . $user_id . '"');
    if ($result->num_rows() > 0) {
        return $result->row()->fullname;
    }
    return '<span class="text-red">Unknown</span>';
}

function gen_site($site_id) {
    $sites = [
        0 => 'All Network', // All network
        1 => 'Nextren', // Nextren
        2 => 'Nakita', // Nakita
        3 => 'Intisari', // Intisari
        4 => 'Grid', // Grid
        5 => 'Nova', // Nova
        6 => 'CewekBanget', // Cewekbanget
        7 => 'Hai', // Hai
        8 => 'Bobo', // Bobo
        9 => 'iDEA', // Idea
        10 => 'Sajian Sedap', // Sajiansedap
        11 => 'Fotokita', // Fotokita
        12 => 'Info Komputer', // Infokomputer
        13 => 'National Geographic', // Nationalgeographic
        14 => 'Stylo', // Stylo,
        15 => 'Grid Games',
        16 => 'Bolanews'
    ];

    return $sites[$site_id];
    // switch ($site_id) {
    //     case '1':
    //         $name = 'Nextren';
    //         break;
    //     case '2':
    //         $name = 'Nakita';
    //         break;
    //     default:
    //         $name = 'All Site';
    //         break;
    // }
    // return $name;
}

function total_hits($value, $result) {
    if ($result) {
        foreach ($result as $row) {
            if ($row[0] == $value) {
                return (int) $row[1];
                break;
            }
        }
    } else {
        return 0;
    }
}

function gen_alert($result, $message) {
    $ci = & get_instance();
    $alert = array(
        'type' => 'success',
        'message' => $message
    );
    if (!empty($result['message'])) {
        $alert = array(
            'type' => 'error',
            'message' => $result['message']
        );
    }
    $ci->session->set_flashdata('alert', '<script>swal({type: "' . $alert['type'] . '",title: "' . $alert['message'] . '",showConfirmButton: false,timer: 1500})</script>');
}

function gen_view($data) {
    $ci = & get_instance();
    if (!empty($_SERVER['HTTP_X_PJAX'])) {
        echo $data['content'];
    } elseif ($ci->input->get('popup')) {
        $ci->load->view('template_popup_view', $data);
    } else {
        $ci->load->view('template_view', $data);
    }
}

function toUrlDev($url = "") {
    if (strpos($url, 'nakita.grid.id') != false) {
        $hostname = 'http://nakitadev.grid.id';
        return $hostname . strstr($url, '/read/');
    }
    return $url;
}

function gen_upload_image($name, $path, $path_resize) {
    $ci = & get_instance();
    $config['upload_path'] = $path; //path folder
    $config['allowed_types'] = 'jpg|jpeg|png'; //type yang dapat diakses bisa anda sesuaikan
    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
    $config['max_size'] = 1024;
    $ci->upload->initialize($config);
    if ($ci->upload->do_upload($name)) {
        $gbr = $ci->upload->data();
        //Compress Image
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $gbr['file_name'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = '50%';
        $config['width'] = 300;
        $config['height'] = 200;
        $config['new_image'] = $path_resize . $gbr['file_name'];
        $ci->load->library('image_lib', $config);
        $ci->image_lib->resize();


        $data = array($gbr['file_name'], 'ok');
        return $data;
    } else {
        $data = array($ci->upload->display_errors(), 'no');
        return $data;
    }
}
