<main class="app-content">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-dashboard"></i> <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?></h1>          
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">Master Data / <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?> </a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">          
            <div class="tile">
                <div class="row">
                    <div class="col-md-12">                                       
                    	<?php
                        $attributes = array('class' => 'Frminstansi', 'id' => 'Frminstansi');
                        echo form_open(base_url('backend/kelas/proses'), $attributes);
                        ?>                        
                        <div class="row">
                        	<div class="col-md-12">
                        	<input type="hidden" name="id_kelas" id="id_kelas" value="<?php echo isset($data_edit) ? $data_edit->id_kelas : '' ?>">
                        	<div class="form-group">
                        		<label>Nama Kelas *</label>
                        		<input type="text" name="txtnama" id="txtnama" class="form-control" value="<?php echo isset($data_edit) ? $data_edit->nama_kelas : '' ?>">
                        	</div> 
                            <div class="form-group">
                                <label>Durasi Kelas</label>
                                <input type="number" name="txtdurasi" id="txtdurasi" class="form-control" value="<?php echo isset($data_edit) ? $data_edit->durasi_kelas : '' ?>">
                            </div>                          
                        	</div>
                        </div>                       
                        <div class="tile-footer">
                            <div class="col-12 text-right">
                                <a class="btn btn-danger btn_back" href="javascript:void(0);"><i class="fa fa-arrow-left"></i> Kembali</a>      
                                <button class="btn btn-primary btn_simpan" type="submit"><i class="fa fa-save"></i>Simpan</button>
                            </div>
                        </div>                    
                        </form>
                    </div>              
                </div>
            </div>
        </div>
    </div>    
</main>

<script type="text/javascript">         
    $('.btn_back').click(function () {
        history.back(1);
    });

   
    $('#Frminstansi').on("submit", function (ev) {
        ev.preventDefault();       
         var DIR = '<?php echo base_url(); ?>';
        var fa = new FormData($(this)[0]);                
        $('.btn_simpan,.btn_back').text('Loading...'); //change button text
        $('.btn_simpan,.btn_back').attr('disabled', true); //set button disable            
        $('.remove_tag').remove();
        $('.form-control').removeClass('is-invalid');
        console.log(DIR);
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            data: fa,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                console.log(response);
                if (response.success == 'kosong') {
                    $('.btn_simpan').html('<i class="fa fa-save"></i>Simpan');
                    $('.btn_back').html('<i class="fa fa-arrow-left"></i>Kembali');
                    $('.btn_simpan,.btn_back').attr('disabled', false);
                    $.each(response.msg, function (key, value) {
                        if (value != '') {
                            $("#" + key).addClass('is-invalid');
                            $("#" + key).after('<div class="form-control-feedback remove_tag">' + value + '</div>');
                            $('.error').css("color", "#d61629");
                        }
                    });
                    return;
                }

                swal({title: response.msg.title, type: response.msg.type, text: response.msg.text, timer: 2000, showConfirmButton: false});

                if (response.success == true) {
                    setTimeout(function () {
                        history.back(1);
                    }, 2000);
                } else {
                    $('.btn_simpan').html('<i class="fa fa-save"></i>Simpan');
                    $('.btn_back').html('<i class="fa fa-arrow-left"></i>Kembali');
                    $('.btn_simpan,.btn_back').attr('disabled', false);
                }


            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.btn_simpan').html('<i class="fa fa-save"></i>Simpan');
                $('.btn_back').html('<i class="fa fa-arrow-left"></i>Kembali');
                $('.btn_simpan,.btn_back').attr('disabled', false);
            }
        });
    });
</script>