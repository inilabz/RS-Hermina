<main class="app-content">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-dashboard"></i> <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?></h1>          
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">Master Data / <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?> </a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <p><a class="btn btn-primary icon-btn" href="<?php echo site_url('backend/kelas/add'); ?>"><i class="fa fa-plus"></i>Tambah <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?></a></p>

                    <table class="table table-hover table-bordered" id="sampleTable">                         
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Kelas</th>
                                <th>Durasi Kelas</th>                      
                                <th style="width: 23%;">Action</th>                  
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $html = '';
                            $uri = $this->uri->segment(2);
                            foreach ($item as $row) {
                               if($row->status == 0){
                               	$button = '<a href = "javascript:void(0)" class = "btn btn-danger btn-sm btn_delete" data-id = "' . $row->id_kelas . '" data-block ="' . $row->status . '" data-nama = "' . $row->nama_kelas . '"><i class = "fa fa-remove"></i></a>';
                               }else{
                               	$button = '<a href = "javascript:void(0)" class = "btn btn-info btn-sm btn_delete" data-id = "' . $row->id_kelas . '" data-block ="' . $row->status . '" data-nama = "' . $row->nama_kelas . '"><i class = "fa fa-check"></i></a>';
                               }

                               $html .= '<tr>';
                               $html .= '<td>'.$no++.'</td>';
                               $html .= '<td>'.$row->nama_kelas.'</td>';
                               $html .= '<td>'.$row->durasi_kelas.' Menit</td>';
                               $html .= '<td>';
                               $html .= '<a href ="' . base_url() . 'backend/kelas/edit/' . $row->id_kelas . '" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a> ';

                               $html .= $button;

                               $html .= '</td>';
                               $html .= '</tr>';
                            }
                            echo $html;
                            ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<script>    
    
    $(document).ready(function () {
        $('#sampleTable').DataTable();
    });
    $('.btn_delete').on('click', function () {
        var id = $(this).data('id');        
        var data_block = $(this).attr('data-block');               
        var nama = $(this).attr('data-nama');
        if (data_block > 0) {
            var text = "Membuka Block  " + nama;
        } else {
            var text = "Memblock  " + nama;
        }        
        var uri = '<?php echo $this->uri->segment(2); ?>';
        swal({
            title: "Apakah Anda Yakin ?",
            text: "Ingin " + text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yakin',
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?php echo site_url() . 'backend/kelas/block_kelas'; ?>',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        'id': id,
                        'data_block': data_block,
                        '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                    },
                    success: function (result) {
                        console.log(result);
                        swal("Berhasil!", "Berhasil " + text, "success");
                        location.reload();
                    }
                });
            } else {
                swal("Batal", "Batal " + text, "error");
            }
        });
    });

</script>