<main class="app-content">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-dashboard"></i> <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?></h1>          
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">Master Data / <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?> </a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <p><a class="btn btn-primary icon-btn" href="<?php echo site_url('backend/cetak'); ?>"><i class="fa fa-download"></i>Cetak <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?></a></p>

                    <table class="table table-hover table-bordered" id="sampleTable">                         
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Karyawan</th>
                                <th>Total Durasi Kehadiran</th>                                
                                <!-- <th style="width: 23%;">Action</th>  -->                 
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $html_absen = '';
                                $no = 1;
                                foreach ($item as $key) {                                                                    
                                    $html_absen .= '<tr>';
                                    $html_absen .= '<td>'.$no++.'</td>';
                                    $html_absen .= '<td>'.$key->user_nama.'</td>';                                    
                                    $html_absen .= '<td>'.$key->total_hadir.' Menit</td>';
                                    // $html_absen .= '<td><a href="'.base_url('backend/absen/detail').'/'.$key->id_absen.'/'.$key->id_kelas.'">detail</a></td>';
                                    $html_absen .= '</tr>';
                                }
                                echo $html_absen;
                            ?>
                         
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<script>    
    
    $(document).ready(function () {
        $('#sampleTable').DataTable();
    });
   
</script>