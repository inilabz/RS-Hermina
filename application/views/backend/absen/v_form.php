
<main class="app-content">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-dashboard"></i> Input Data Absensi Tanggal <?php echo date('Y-m-d'); ?></h1>          
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">Master Data / <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?> </a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">          
            <div class="tile">
                <div class="row">
                    <div class="col-md-12">                                       
                    	<?php
                        $attributes = array('class' => 'Frminstansi', 'id' => 'Frminstansi');
                        echo form_open(base_url('backend/absen/proses'), $attributes);
                        ?>                        
                        <div class="row">
                        	<div class="col-md-12">                        	
                        	<div class="form-group">
                        		<label>Nama Kelas *</label>
                        		<select class="form-control" name="txtkelas" id="kelas">
                        			<option value="">-- Pilih Kelas --</option>
                        			<?php foreach ($kelas as $key ) { ?>                        					
                        				<option value="<?php echo $key->id_kelas ?>"><?php echo $key->nama_kelas; ?></option>
                        				<?php } ?>
                        		</select>
                        	</div>                                                       
                        	</div>
                        </div>                       
                        <div class="result_html"></div>          
                        </form>
                    </div>              
                </div>
            </div>
        </div>
    </div>    
</main>
<script type="text/javascript">
	$(document).ready(function(){		

        $('#kelas').change(function(){  
            var token_name = '<?php echo $name_token; ?>';
            var token_value = '<?php echo $value_token; ?>';          
            $('.tile-footer').remove();
            $.ajax({
            url: '<?php echo base_url() ?>backend/absen-get-kelas',
            type: 'post',
            data: 'id_kelas='+this.value+'&'+token_name+'='+token_value,           
            dataType: 'json',
            success: function (response) {        
                var html = '<table class="table">';
                    html += '<thead>';
                    html += '<tr>';
                    html += '<th>No</th>';
                    html += '<th>Nama</th>';
                    html += '<th>Email</th>';
                    html += '<th>Action</th>';
                    html += '</tr>';
                    html += '</thead>';
                    html += '<tbody>';        
                if(response.data_karyawan.total > 0){                   
                    var no = 1;
                    for (var i = 0; i < response.data_karyawan.total; i++) {
                        var data_value = response.data_karyawan.data[i]['user_id']+'|';
                        html += '<tr>';
                        html += '<td>'+no+'</td>';
                        html += '<td>'+response.data_karyawan.data[i]['user_nama']+'</td>';
                        html += '<td>'+response.data_karyawan.data[i]['user_email']+'</td>';
                        html += '<td>';
                        html += '<input type="radio" name="'+response.data_karyawan.data[i]['user_id']+'" value="A">A <span></span>';
                        html += '<input type="radio" name="'+response.data_karyawan.data[i]['user_id']+'" value="I">I <span></span>';
                        html += '<input type="radio" name="'+response.data_karyawan.data[i]['user_id']+'" value="S">S <span></span>';
                        html += '<input type="radio" name="'+response.data_karyawan.data[i]['user_id']+'" value="M">M <span></span>';
                        html += '<input type="radio" name="'+response.data_karyawan.data[i]['user_id']+'" value="N">N <span></span>';
                        html += '</td>';
                        html += '</tr>';
                        no++;
                    }
                    var button = '<div class="tile-footer">';
                        button +='<div class="col-12 text-right">';
                        button +='<a class="btn btn-danger btn_back" href="<?php echo base_url('backend/absen'); ?>"><i class="fa fa-arrow-left"></i> Kembali</a> ';
                        button +='<button class="btn btn-primary btn_simpan" type="submit"><i class="fa fa-save"></i>Simpan</button>';
                        button +='</div>';
                        button +='</div>';
                        $('.result_html,.tabel').after(button);
                   
                }else{
                        html += '<tr>';
                        html += '<td colspan="5"><center>Data Masih Kosong</center></td>';                      
                        html += '</tr>';
                }                                 
                html += '</tbody>';                
                html += '</tabel>';                
                
                $('.result_html').html(html);
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
               
            }
        });
        });
		
        
	});
</script>