<main class="app-content">
    <div class="app-title">
        <div><h1><i class="fa fa-dashboard"></i> <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?></h1></div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">Master Data / <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?> </a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
            	<center>
            		<h2>Detail Absen <?php echo $kelas->nama_kelas ?></h2><br>Tanggal <?php echo $kelas->nama_kelas ?>            		

            	</center>
                <div class="tile-body">
                    <div class="row">
                        <table class="table">
                        	<thead>
                        		<tr>
                        			<th>No</th>
                        			<th>Nama</th>
                        			<th>Email</th>
                        			<th>Keterangan</th>
                        		</tr>
                        	</thead>
                        	<tbody>
                        		<?php
                        			$html_res = '';
                        			$no = 1;
                        			$keterangan = '';
                        			foreach ($data_karyawan as $key) {
                        				if($key->keterangan == 'A'){
                        					$keterangan = 'Alpha';
                        				}else if($key->keterangan == 'I'){
                        					$keterangan = 'Ijin';
                        				}else if($key->keterangan == 'S'){
                        					$keterangan = 'Sakit';
                        				}else if($key->keterangan == 'M'){
                        					$keterangan = 'Masuk';
                        				}else if($key->keterangan == 'N'){
                        					$keterangan = 'Belum Ada keterangan';
                        				}
                        				$html_res .= '<tr>';
                        				$html_res .= '<td>'.$no++.'</td>';
                        				$html_res .= '<td>'.$key->user_nama.'</td>';
                        				$html_res .= '<td>'.$key->user_email.'</td>';
                        				$html_res .= '<td>'.$keterangan.'</td>';
                        				$html_res .= '<tr>';
                        			}
                        			echo $html_res;
                        		?>
                        	</tbody>
                        </table>
                    </div>
                </div>
                <div class="col-12 text-right">
                    <a class="btn btn-danger btn_back" href="<?php echo base_url('backend/absen'); ?>"><i class="fa fa-arrow-left"></i> Kembali</a>
                </div>
            </div>
        </div>
    </div>
</main>