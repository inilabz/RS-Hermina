<!-- Sidebar menu-->    
<aside class="app-sidebar">
    <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image">
        <div>
            <p class="app-sidebar__user-name"><?php echo isset($user_login_nama) ? $user_login_nama : '' ?>    </p>
            <p class="app-sidebar__user-designation"><?php echo isset($user_access_name) ? ucfirst($user_access_name) : '' ?></p>
        </div>
    </div>
    <ul class="app-menu"> 
        <?php $list_master_data = array('section', 'user', 'member', 'instansi','kelas','pengajar','absen','laporan');?>
        <li class="treeview <?php echo in_array($this->uri->segment(2), $list_master_data) ? 'is-expanded' : ''; ?>">
            <a class="app-menu__item" href="#" data-toggle="treeview">
                <i class="app-menu__icon fa fa-laptop"></i>
                <span class="app-menu__label">Master Data</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a class="treeview-item <?php echo $this->uri->segment(2) == 'user' ? 'active' : '' ?>" href="<?php echo base_url('backend/user'); ?>"><i class="icon fa fa-circle-o"></i>List Admin</a></li>

                <li><a class="treeview-item <?php echo $this->uri->segment(2) == 'member' ? 'active' : '' ?>" href="<?php echo base_url('backend/member'); ?>"><i class="icon fa fa-circle-o"></i>List Karyawan</a></li> 

                <li><a class="treeview-item <?php echo $this->uri->segment(2) == 'pengajar' ? 'active' : '' ?>" href="<?php echo base_url('backend/pengajar'); ?>"><i class="icon fa fa-circle-o"></i>List Pengajar</a></li> 
                  
                <li><a class="treeview-item <?php echo $this->uri->segment(2) == 'instansi' ? 'active' : '' ?>" href="<?php echo base_url('backend/instansi'); ?>"><i class="icon fa fa-circle-o"></i>List Instansi</a></li>
                <li><a class="treeview-item <?php echo $this->uri->segment(2) == 'kelas' ? 'active' : '' ?>" href="<?php echo base_url('backend/kelas'); ?>"><i class="icon fa fa-circle-o"></i>List Kelas</a></li>
                <li><a class="treeview-item <?php echo $this->uri->segment(2) == 'absen' ? 'active' : '' ?>" href="<?php echo base_url('backend/absen'); ?>"><i class="icon fa fa-circle-o"></i>Abses</a></li> 
                <li><a class="treeview-item <?php echo $this->uri->segment(2) == 'laporan' ? 'active' : '' ?>" href="<?php echo base_url('backend/laporan'); ?>"><i class="icon fa fa-circle-o"></i>Laporan</a></li>   
            </ul>
        </li>		
    </ul>
</aside>