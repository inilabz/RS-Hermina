<main class="app-content">
    <div class="app-title">
        <div><h1><i class="fa fa-dashboard"></i> <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?></h1></div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">Master Data / <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?> </a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-8">                                                 
                            <table class="table">
                                <tr>
                                    <th>Nama Lengkap</th>
                                    <th>: <?php echo ucwords($detail->user_nama); ?></th>                                                       
                                </tr>
                                <tr>
                                    <th>NIK</th>
                                    <th>: <?php echo ucwords($detail->user_nik); ?></th>                                                       
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <th>: <?php echo $detail->user_email; ?></th>
                                </tr>
                                <tr>
                                    <th>Kontak</th>
                                    <th>: <?php echo $detail->user_phone; ?></th>
                                </tr>
                                <tr>
                                    <th>Kontak Rumah</th>
                                    <th>: <?php echo $detail->user_phone_rumah; ?></th>
                                </tr>
                                <tr>
                                    <th>Gender</th>
                                    <th>: <?php echo $detail->user_gender == 0 ? 'Wanita' : 'Laki-laki'; ?></th>
                                </tr>
                                <tr>
                                    <th>Alamat</th>
                                    <th>: <?php echo $detail->user_alamat; ?></th>
                                </tr>
                                <tr>
                                    <th>Tanggal lahir</th>
                                    <?php
                                    $biday = new DateTime($detail->user_tanggal_lahir);
                                    $today = new DateTime();
                                    $diff = $today->diff($biday);
                                    $umur = $diff->y > 0 ? '[' . $diff->y . ' Tahun]' : '';
                                    ?>
                                    <th>: <?php echo date('d F Y', strtotime($detail->user_tanggal_lahir)) . '&nbsp;' . $umur; ?></th>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <?php
                                    if ($detail->delete_by != 0 && $detail->delete_at != '0000-00-00 00:00:00') {
                                        $button_status = 'Block';
                                    } else {
                                        if ($detail->user_status == 1) {
                                            $button_status = 'Belum Aktif';
                                        } else if ($detail->user_status == 2) {
                                            $button_status = 'Aktif';
                                        } else {
                                            $button_status = 'Block';
                                        }
                                    }
                                    ?>
                                    <th>: <?php echo $button_status; ?></th>
                                </tr>
                                <tr>
                                    <th>Status Login</th>
                                    <th>: <?php echo $detail->user_status_login == 1 ? 'Member' : 'Admin'; ?></th>
                                </tr>                                                               
                            </table>
                        </div>
                        <div class="col-md-4">
                            <?php
                            if (!empty($detail->user_photo)) {
                                echo "<img id='img-upload' class='img-responsive' src='" . base_url("/assets/frontend/photo_profile/resize/") . $detail->user_photo . "'/>";
                            }
                            ?>   
                        </div>
                    </div>
                </div>
                <div class="col-12 text-right">
                    <a class="btn btn-danger btn_back" href="javascript:history.go(-1)"><i class="fa fa-arrow-left"></i> Kembali</a>                
                    <a class="btn btn-primary" href="<?php echo base_url() . 'backend/' . $this->uri->segment(2) . '/edit/' . $detail->user_id; ?>"><i class="fa fa-pencil"></i> Update</a>                    
                </div>
            </div>
        </div>
    </div>
</main>