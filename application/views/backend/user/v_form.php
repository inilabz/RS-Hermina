

<main class="app-content">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-dashboard"></i> <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?></h1>          
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">Master Data / <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?> </a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">          
            <div class="tile">
                <div class="row">
                    <div class="col-md-12">                
                        <?php
                        $attributes = array('class' => 'FrmMember', 'id' => 'FrmMember');
                        echo form_open($url_proses, $attributes);
                        ?>
                        <div class="row">

                            <div class="col-md-9">                                
                                <?php
                                    if($this->uri->segment(2) == 'member'){
                                        echo "<input type='hidden' name='user_akses' id='user_akses' value='1'>";
                                    }else if($this->uri->segment(2) == 'pengajar'){
                                        echo "<input type='hidden' name='user_akses' id='user_akses' value='3'>";
                                    }else{
                                        echo "<input type='hidden' name='user_akses' id='user_akses' value='2'>";
                                    }
                                 ?>                                

                                <div class="row">
                                    <div class="col-md-6">     
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Lengkap *</label>
                                    <input type="hidden" name="user_id" id="user_id" value="<?php echo isset($data_edit->user_id) ? $data_edit->user_id : ''; ?>">
                                    <input class="form-control" name="user_nama_lengkap" id="user_nama_lengkap" type="text" aria-describedby="nama" placeholder="Nama Lengkap" value="<?php echo isset($data_edit->user_nama) ? $data_edit->user_nama : ''; ?>">
                                </div>                                                       
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email *</label>
                                            <input class="form-control" name="user_email" id="user_email" type="email" aria-describedby="nama" placeholder="Email" value="<?php echo isset($data_edit->user_email) ? $data_edit->user_email : ''; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Telepon</label>
                                            <input class="form-control" name="user_phone" id="user_phone" type="number" aria-describedby="nama" placeholder="Telepon" value="<?php echo isset($data_edit->user_phone) ? $data_edit->user_phone : ''; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Jenis Kelamin *</label>
                                            <select class="form-control" name="user_gender" id="user_gender">
                                                <option value="">-- Pilih Jenis Kelamin --</option>
                                                <option value="0" <?php echo isset($data_edit->user_gender) && $data_edit->user_gender == 0 ? 'selected' : ''; ?>>Wanita</option>
                                                <option value="1" <?php echo isset($data_edit->user_gender) && $data_edit->user_gender == 1 ? 'selected' : ''; ?>>Pria</option>
                                            </select>
                                        </div> 
                                        <div class="form-group">
                                    <label>Instansi *</label>
                                    <select class="form-control" name="user_instansi" id="user_instansi">
                                        <option value="">-- Pilih Instansi --</option>
                                        <?php foreach ($instansi as $key) { ?>
                                              <option value="<?php echo $key->id_instansi ?>" <?php echo isset($data_edit->user_gender) && $data_edit->id_instansi == $key->id_instansi ? 'selected' : ''; ?>> <?php echo $key->nama_instansi; ?></option>  
                                        <?php } ?>
                                    </select>
                                </div>                              
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">NIK *</label>
                                            <input type="text" name="user_nik" id="user_nik" class="form-control" placeholder="NIK" value="<?php echo isset($data_edit->user_nik) ? $data_edit->user_nik : ''; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Password *</label>
                                            <input class="form-control" name="user_password" id="user_password" type="password" aria-describedby="nama" placeholder="Password" value="<?php echo isset($data_edit->user_show_pass) ? $data_edit->user_show_pass : ''; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Telepon Rumah</label>
                                            <input class="form-control" name="user_phone_rumah" id="user_phone_rumah" type="text" aria-describedby="nama" placeholder="Telepon Rumah" value="<?php echo isset($data_edit->user_phone_rumah) ? $data_edit->user_phone_rumah : ''; ?>">
                                        </div>
                                        <div class="form-group">
                                    <label for="exampleInputEmail1">Tanggal Lahir</label>
                                    <input class="form-control" autocomplete="off" name="user_tanggal_lahir" id="user_tanggal_lahir" type="text" aria-describedby="nama" placeholder="Tanggal Lahir" value="<?php echo isset($data_edit->user_tanggal_lahir) ? $data_edit->user_tanggal_lahir : ''; ?>">
                                </div>   
                                <div class="form-group">
                                    <label>Kelas *</label>
                                    <select class="form-control" name="user_kelas[]" id="user_kelas" multiple="">
                                        <option value=""></option>
                                        <?php 
                                            $html_kelas = '';
                                            
                                            foreach ($kelas as $kelas_row) {
                                               if(!empty($data_edit_kelas)){
                                                    $selected = '';
                                                    if(in_array($kelas_row->id_kelas,$data_edit_kelas)){
                                                        $selected .= 'selected';
                                                    }
                                                }
                                                $html_kelas .= '<option value="'.$kelas_row->id_kelas.'" '.$selected.'>'.$kelas_row->nama_kelas.'</option>';                                                                                       
                                                
                                            }
                                            echo $html_kelas;
                                        ?>
                                    </select>
                                </div>                             
                                    </div>
                                </div> 
                                                                                                                       
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Alamat Tinggal</label>
                                    <textarea class="form-control" rows="4" name="user_alamat" id="user_alamat"><?php echo isset($data_edit->user_alamat) ? $data_edit->user_alamat : ''; ?></textarea>                            
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="txtlabel">Upload Image</label>
                                    <small class="form-text text-muted">Ukuran photo Max 2 MB dan Format JPG,JPEG, atau PNG </small>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                Browse… <input type="file" id="imgInp" name="foto">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly>
                                    </div>
                                    <?php
                                    if (!empty($data_edit->user_photo)) {
                                        echo "<img id='img-upload' class='' src='" . base_url("/assets/frontend/photo_profile/resize/") . $data_edit->user_photo . "'/>";
                                    } else {
                                        echo "<img id='img-upload' class=''/>";
                                    }
                                    ?>                                    
                                </div>                                
                            </div>
                        </div>                       
                        <div class="tile-footer">
                            <div class="col-12 text-right">
                                <a class="btn btn-danger btn_back" href="javascript:void(0);"><i class="fa fa-arrow-left"></i> Kembali</a>                
                                <button class="btn btn-primary btn_simpan" type="submit"><i class="fa fa-save"></i>Simpan</button>
                            </div>
                        </div>                    
                        </form>
                    </div>              
                </div>
            </div>
        </div>
    </div>    
</main>

<script type="text/javascript">
    $(document).ready(function () {
        $('#user_tanggal_lahir').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#user_kelas').select2({
            allowClear: true,
            placeholder: 'Pilih Kelas',            
        });
    });
    $(document).on('change', '.btn-file :file', function () {
        var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function (event, label) {
        var input = $(this).parents('.input-group').find(':text'),
                log = label;
        if (input.length) {
            input.val(log);
        } else {
            if (log)
                alert(log);
        }

    });
    $('.btn_back').click(function () {
        history.back(1);
    });


    $("#imgInp").change(function () {
        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            console.log(reader);

            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
                $('#img-upload').addClass('img-thumbnail');
                $('#img-upload').css('margin-top', '6px');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $('#FrmMember').on("submit", function (ev) {
        ev.preventDefault();
        var DIR = '<?php echo base_url(); ?>';
        var fa = new FormData($(this)[0]);
        $('.btn_simpan,.btn_back').text('Loading...'); //change button text
        $('.btn_simpan,.btn_back').attr('disabled', true); //set button disable            
        $('.remove_tag').remove();
        $('.form-control').removeClass('is-invalid');
        $.ajax({
            url: DIR + 'backend/user/proses/user',
            type: 'post',
            data: fa,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                console.log(response);
                if (response.success == 'kosong') {
                    $('.btn_simpan').html('<i class="fa fa-save"></i>Simpan');
                    $('.btn_back').html('<i class="fa fa-arrow-left"></i>Kembali');
                    $('.btn_simpan,.btn_back').attr('disabled', false);
                    $.each(response.msg, function (key, value) {
                        if (value != '') {
                            $("#" + key).addClass('is-invalid');
                            $("#" + key).after('<div class="form-control-feedback remove_tag">' + value + '</div>');
                            $('.error').css("color", "#d61629");
                        }
                    });
                    return;
                }

                swal({title: response.msg.title, type: response.msg.type, text: response.msg.text, timer: 2000, showConfirmButton: false});

                if (response.success == true) {
                    setTimeout(function () {
                        history.back(1);
                    }, 2000);
                } else {
                    $('.btn_simpan').html('<i class="fa fa-save"></i>Simpan');
                    $('.btn_back').html('<i class="fa fa-arrow-left"></i>Kembali');
                    $('.btn_simpan,.btn_back').attr('disabled', false);
                }


            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.btn_simpan').html('<i class="fa fa-save"></i>Simpan');
                $('.btn_back').html('<i class="fa fa-arrow-left"></i>Kembali');
                $('.btn_simpan,.btn_back').attr('disabled', false);
            }
        });
    });
</script>