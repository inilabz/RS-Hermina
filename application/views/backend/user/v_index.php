<main class="app-content">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-dashboard"></i> <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?></h1>          
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">Master Data / <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?> </a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <p><a class="btn btn-primary icon-btn" href="<?php echo base_url($button_add); ?>"><i class="fa fa-plus"></i>Tambah <?php echo ucwords(str_replace('-', ' ', $this->uri->segment(2))); ?></a></p>

                    <table class="table table-hover table-bordered" id="sampleTable">                         
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Lengkap</th>
                                <th>Email</th>
                                <th>Gender</th>
                                <th style="width: 23%;">Action</th>                  
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $html = '';
                            $uri = $this->uri->segment(2);
                            foreach ($item as $row) {
                                if ($row->delete_by != 0 && $row->delete_at != '0000-00-00 00:00:00') {
                                    $button_block = ' <a href = "javascript:void(0)" class = "btn btn-danger btn-sm btn_delete" data-id = "' . $row->user_id . '" data-block ="' . $row->delete_by . '" data-nama = "' . $row->user_nama . '"><i class = "fa fa-remove"></i></a></td>';
                                } else {
                                    if ($row->user_status == 1) {
                                        $button_block = ' <a href = "javascript:void(0)" class = "btn btn-secondary btn-sm"><i class = "fa fa-exclamation-triangle"></i>Belum</a></td>';
                                    } else if ($row->user_status == 2) {
                                        $button_block = ' <a href = "javascript:void(0)" class = "btn btn-info btn-sm btn_delete" data-id = "' . $row->user_id . '" data-block ="' . $row->delete_by . '" data-nama = "' . $row->user_nama . '"><i class = "fa fa-check"></i></a></td>';
                                    } else {
                                        $button_block = ' <a href = "javascript:void(0)" class = "btn btn-danger btn-sm btn_delete" data-id = "' . $row->user_id . '" data-block ="' . $row->delete_by . '" data-nama = "' . $row->user_nama . '"><i class = "fa fa-remove"></i></a></td>';
                                    }
                                }
                                $html .= '<tr>';
                                $html .= '<td>' . $no++ . '</td>';
                                $html .= '<td>' . $row->user_nama . '</td>';
                                $html .= '<td>' . $row->user_email . '</td>';
                                $html .= '<td>' . ($row->user_gender == 1 ? "Pria" : "Wanita") . '</td>';
                                $html .= '<td>';
                                $html .= '<a href ="' . base_url() . 'backend/' . $uri . '/detail/' . $row->user_id . '" class="btn btn-warning btn-sm"><i class="fa fa-eye"></i></a>';
                                $html .= ' <a href ="' . base_url() . 'backend/' . $uri . '/edit/' . $row->user_id . '" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>';
                                $html .= $button_block;
                                $html .= '</tr>';
                            }
                            echo $html;
                            ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<script>    
    
     $(document).ready(function () {
            $('#sampleTable').DataTable();
        });
    $('.btn_delete').on('click', function () {
        var id = $(this).data('id');
        var data_block = $(this).attr('data-block');
        var nama = $(this).attr('data-nama');
        if (data_block > 0) {
            var text = "Membuka Block  " + nama;
        } else {
            var text = "Memblock  " + nama;
        }
        var token = $('#token').val();
        var uri = '<?php echo $this->uri->segment(2); ?>';
        swal({
            title: "Apakah Anda Yakin ?",
            text: "Ingin " + text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yakin',
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?php echo site_url() . 'backend/user/proses_hapus_'; ?>' + uri,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        'id': id,
                        'data_block': data_block,
                        '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                    },
                    success: function (result) {
                        console.log(result);
                        swal("Berhasil!", "Berhasil " + text, "success");
                        location.reload();
                    }
                });
            } else {
                swal("Batal", "Batal " + text, "error");
            }
        });
    });

</script>