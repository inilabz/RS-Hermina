<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">    
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/backend/css/main.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/sweetalert/sweetalert.css'); ?>">    
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="<?php echo base_url('assets/backend/js/jquery-3.2.1.min.js'); ?>"></script>
        <title>Login - Admin Hermina</title>
    </head>
    <body>
        <section class="material-half-bg">
            <div class="cover"></div>
        </section>
        <section class="login-content">
            <div class="logo">
                <h1 style="text-align: center;">Hermina</h1>
            </div>
            <div class="login-box">
                <?php
                $attributes = array('class' => 'login-form', 'id' => 'FrmLogin');
                echo form_open('backend/pro/login', $attributes);
                ?>
                <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>Admin</h3>
                <div class="form-group">          
                    <label class="control-label">EMAIL</label>
                    <input class="form-control email" type="text" placeholder="Email" autofocus id="email" name="email">          
                </div>
                <div class="form-group">
                    <label class="control-label">PASSWORD</label>
                    <input class="form-control password" type="password" placeholder="Password" id="password" name="password">
                </div>
                <div class="form-group btn-container">
                    <button type="submit" class="btn btn-primary btn-block btn_login"><i class="fa fa-sign-in fa-lg fa-fw"></i>MASUK</button>
                </div>
                </form>
            </div>
        </section>
        <script src="<?php echo base_url('assets/backend/js/popper.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/backend/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/backend/js/main.js'); ?>"></script>  
        <script src="<?php echo base_url('assets/backend/js/plugins/pace.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/sweetalert/sweetalert-dev.js'); ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.login-content [data-toggle="flip"]').click(function () {
                    $('.login-box').toggleClass('flipped');
                    return false;
                });

                $('#FrmLogin').on("submit", function (ev) {
                    ev.preventDefault();
                    var fa = $(this);
                    $('.btn_login').text('LOADING...'); //change button text
                    $('.btn_login').attr('disabled', true); //set button disable            
                    $('.remove_tag').remove();
                    $('.form-control').removeClass('is-invalid');
                    $.ajax({
                        url: fa.attr('action'),
                        type: 'post',
                        data: fa.serialize(),
                        dataType: 'json',
                        success: function (response) {
                            console.log(response);
                            if (response.success == 'kosong') {
                                $('.btn_login').text('MASUK');
                                $('.btn_login').attr('disabled', false);
                                $.each(response.msg, function (key, value) {
                                    if (value != '') {
                                        $("#" + key).addClass('is-invalid');
                                        $("#" + key).after('<div class="form-control-feedback remove_tag">' + value + '</div>');
                                        $('.error').css("color", "#dc3545");
                                    }
                                });
                                return;
                            }
                            if (response.success == 'gagal') {
                                $('.btn_login').text('MASUK');
                                $('.btn_login').attr('disabled', false);
                            }

                            swal({title: response.msg.title, type: response.msg.type, text: response.msg.text, timer: 2000, showConfirmButton: false});

                            if (response.success == true) {
                                setTimeout(function () {
                                    window.location = '<?php echo base_url(); ?>' + response.msg.link;
                                }, 2000);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $('.btn_login').text('MASUK');
                            $('.btn_login').attr('disabled', false);
                        }
                    });
                });
            });
        </script>
    </body>
</html>